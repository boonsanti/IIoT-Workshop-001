#include <ModbusMaster.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>

#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>

const char* mqtt_server = "broker.hivemq.com";

// instantiate ModbusMaster object
ModbusMaster node;
const int ledPin = 2;

WiFiClient espClient;
PubSubClient client(espClient);
long lastMsg = 0;
char msg[50];

void setup()
{
  // use Serial (port 0); initialize Modbus communication baud rate
  Serial.begin(19200, SERIAL_8E1);

  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, 0);

  WiFiManager wifiManager;

  wifiManager.autoConnect("AutoConnectAP");

  // communicate with Modbus slave ID 1 over Serial (port 0)
  node.begin(1, Serial);
  
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
}

void callback(char* topic, byte* payload, unsigned int length) {
  String t = String(topic);
  String data = String((char *)payload);
  if (t == "ESPXXXXXX/Y0") {
    node.writeSingleCoil(0x4800, (uint8_t)data.toInt());
  }
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    // Attempt to connect
    if (client.connect("ESPXXXXXX")) {
      client.subscribe("ESPXXXXXX/Y0");
    } else {
      delay(5000);
    }
  }
}

void loop()
{
  uint8_t result;
  uint16_t data;
  long now = millis();

  if (!client.connected()) {
    reconnect();
  }
  client.loop();
  
  // slave: read (6) 16-bit registers starting at register 2 to RX buffer
  result = node.readCoils(0x4000, 1);
  
  // do something with data if read is successful
  if (result == node.ku8MBSuccess)
  {
    data = node.getResponseBuffer(0);
  
    if (now - lastMsg > 1000) {
      lastMsg = now;
      snprintf (msg, 16, "%ld", data);
      client.publish("ESPXXXXXX/X0", msg);
    }
    digitalWrite(ledPin, 1);
  } else {
    digitalWrite(ledPin, 0);
  }
}
